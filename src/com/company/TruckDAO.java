package com.company;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.*;

public class TruckDAO extends truck_shopping{
    SessionFactory factory = null;
    Session session = null;

    private static TruckDAO single_instance = null;

    TruckDAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static TruckDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new TruckDAO();
        }

        return single_instance;
    }

    /** Used to get more than one customer from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<truck_shopping> getTrucks() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.company.truck_shopping";
            List<truck_shopping> cs = (List<truck_shopping>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single customer from database */
    public truck_shopping getTrucks(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.company.truck_shopping where id= " + Integer.toString(id);
            truck_shopping c = (truck_shopping) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }


    public truck_shopping saveTrucks(String brand, String model, int year, int price, String vin){
        try{
            truck_shopping truck = new truck_shopping();
            truck.setVin(getVin());
            truck.setPrice(getPrice());
            truck.setYear(getYear());
            truck.setModel(getModel());
            truck.setBrand(getBrand());

            session = factory.openSession();
            session.beginTransaction();
            session.save(truck);
            session.beginTransaction().commit();
            return truck;
        } catch (Exception f){
            f.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }


}
