package com.company;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


import java.util.InputMismatchException;
import java.util.Scanner;

import java.util.List;

public class main {

    public static SessionFactory factory = new Configuration().configure().buildSessionFactory();

    private static TruckDAO single_instance = null;

    void TruckDAO() {
        factory = HibernateUtils.getSessionFactory();
    }

    /**
     * This is what makes this class a singleton.  You use this
     * class to get an instance of the class.
     */
    public static TruckDAO getInstance() {
        if (single_instance == null) {
            single_instance = new TruckDAO();
        }

        return single_instance;
    }

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Want to add another truck to review? Type 1 for yes and 2 for no. " +
                "If you select 2 for no, the list of current trucks for review will display. " +
                "If you select 1 for yes, you will input requested information about the truck " +
                "to be added to the list");
        int answer = input.nextInt();

        Session session = null;

        try {
            if (answer == 1) {
                System.out.print("Please enter the brand name: ");
                String a = input.next();
                System.out.print("\nPlease enter the model: ");
                String b = input.next();
                System.out.print("\nPlease enter the model year: ");
                int c = input.nextInt();
                System.out.print("\nPlease enter the price: ");
                int d = input.nextInt();
                System.out.print("\nPlease enter the vin: ");
                String e = input.next();

                try {
                    truck_shopping truck = new truck_shopping();
                    truck.setBrand(a);
                    truck.setModel(b);
                    truck.setYear(c);
                    truck.setPrice(d);
                    truck.setVin(e);


                    session = factory.openSession();
                    session.beginTransaction();
                    session.save(truck);
                    session.getTransaction().commit();
                } catch (Exception f) {
                    f.printStackTrace();
                    session.getTransaction().rollback();
                } finally {
                    session.close();
                }
                TruckDAO g = TruckDAO.getInstance();
                List<truck_shopping> z = g.getTrucks();
                for (truck_shopping i : z) {
                    System.out.println(i);
                }
            } else {
                TruckDAO t = TruckDAO.getInstance();
                List<truck_shopping> c = t.getTrucks();
                for (truck_shopping i : c) {
                    System.out.println(i);
                }
            }
        } catch (InputMismatchException e) {
            System.out.println("Try to make sure you fill it out exactly as requested.");
        } catch (NumberFormatException e) {
            System.out.println("Enter a number when i ask for a number. Pretty simple dude.");
        } finally {
            System.out.println("Why are trucks so dang expensive?!?!");
        }
    }

}



