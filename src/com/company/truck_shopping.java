package com.company;
import javax.persistence.*;


@Entity
@Table(name = "truck_shopping")
public class truck_shopping {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "brand")
    private String brand;

    @Column(name = "model")
    private String model;

    @Column(name = "year")
    private int year;

    @Column(name = "price")
    private int price;

    @Column(name = "vin", unique = true)
    private String vin;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() { return brand; }

    public void setBrand(String brand) { this.brand = brand; }

    public String getModel() { return model; }

    public void setModel(String model) { this.model = model; }

    public int getYear() { return year; }

    public void setYear(int year) { this.year = year; }

    public int getPrice() { return price; }

    public void setPrice(int price) { this.price = price; }

    public String getVin() { return vin; }

    public void setVin(String vin) { this.vin = vin; }

    public String toString() { return id + ": Make = " + brand + "   Model = " + model + "    Year = " + year + "   Price = " + price + "   Vin = " + vin; }

    public void setBrand() {
    }

    public void setModel() {
    }

    public void setYear() {
    }

    public void setPrice() {
    }

    public void setVin() {
    }
}
